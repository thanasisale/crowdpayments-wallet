=== Piraeus Bank WooCommerce Payment Gateway ===
Contributors: enartia,g.georgopoulos,georgekapsalakis
Author URI: https://www.enartia.com
Tags: ecommerce, woocommerce, payment gateway
Tested up to: 4.9
Requires at least: 4.0
Stable tag: 1.4.0
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Adds Piraeus Bank paycenter as a payment Gateway for WooCommerce

== Description ==
This plugin adds Piraeus Bank paycenter as a payment gateway for WooCommerce. A contract between you and the Bank must be previously signed. Based on original plugin "Piraeus Bank Greece Payment Gateway for WooCommerce" by emspace.gr [https://wordpress.org/plugins/woo-payment-gateway-piraeus-bank-greece/]

It uses the redirect method, and SSL is not required.


Requires SOAP installed in the server / hosting.
== Features ==
Provides pre-auth transactions and free installments.

== Installation ==

Just follow the standard [WordPress plugin installation procedure](http://codex.wordpress.org/Managing_Plugins).

Provide to Piraeus bank at epayments@piraeusbank.gr the following information, in order to provide you with test account information. 
WITH PERMALINKS SET
* Website url :  http(s)://www.yourdomain.gr/
* Referrer url : http(s)://www.yourdomain.gr/checkout/
* Success page :  http(s)://www.yourdomain.gr/wc-api/WC_Piraeusbank_Gateway?peiraeus=success
* Failure page : http(s)://www.yourdomain.gr/wc-api/WC_Piraeusbank_Gateway?peiraeus=fail
* Cancel page : http(s)://www.yourdomain.gr/wc-api/WC_Piraeusbank_Gateway?peiraeus=cancel

WITHOUT PERMALINKS (MODE=SIMPLE)
* Website url :  http(s)://www.yourdomain.gr/
* Referrer url : http(s)://www.yourdomain.gr/checkout/
* Success page :  http(s)://www.yourdomain.gr/?wc-api=WC_Piraeusbank_Gateway&peiraeus=success
* Failure page : http(s)://www.yourdomain.gr/?wc-api=WC_Piraeusbank_Gateway&peiraeus=fail
* Cancel page : http(s)://www.yourdomain.gr/?wc-api=WC_Piraeusbank_Gateway&peiraeus=cancel

Response method : GET
Your's server IP Address 

=== HTTP Proxy ===
In case your server doesn't provide a static IP address for your website, you can use an HTTP Proxy for outgoing requests from the server to the bank. The following fields need to be filled for http proxying:
HTTP Proxy Hostname: Required. If empty then HTTP Proxy is not used.
HTTP Proxy Port: Required if HTTP Proxy Hostname is filled.
HTTP Proxy Login Username/Password: Optional.



== Frequently asked questions ==


== Changelog ==

= 1.4.0 =
New Piraeus API encryption algorithm

= 1.3 =
Added Proxy configuration option.

= 1.0.6 =
WooCommerce backwards compatible

= 1.0.4 =
WooCommerce 3.0 compatible

= 1.0.3 =
Text changed. New Title[GR]: Με κάρτα μέσω Πειραιώς

= 1.0.2 =
Bug Fixes

= 1.0.1 =
Bug Fixes

= 1.0.0 =
Initial Release

